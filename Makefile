# SPDX-License-Identifier: GPL-2.0-only

subdir-ccflags-y +=  -I$(srctree)/vendor/include
subdir-ccflags-y +=  -I$(srctree)/vendor/include/uapi

subdir-ccflags-y +=  -I$(srctree)/vendor/arch/arm64/include
subdir-ccflags-y +=  -I$(srctree)/vendor/include/soc/hobot

subdir-ccflags-y +=  -I$(srctree)/vendor/dirvers/iommu

obj-y	+= drivers/
obj-y	+= sound/
